using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    [TestFixture]
    public class TestesCalculadoraComissao
    {
        [Test]
        public void TesteValorInferiorA10Mil()
        {
            //Arrange
            int valorDaVenda = 5000;
            double comissaoEsperada = 250;
                        
            //Act
            double comissaoCalculada = CalculadoraComissao.CalculaComissao(valorDaVenda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void  TesteValorSuperiorA10Mil(){
             //Arrange
            int valorDaVenda = 100000;
            double comissaoEsperada = 6000;
                        
            //Act
            double comissaoCalculada = CalculadoraComissao.CalculaComissao(valorDaVenda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void  TesteValorIgualA10Mil(){
             //Arrange
            int valorDaVenda = 10000;
            double comissaoEsperada = 500;
                        
            //Act
            double comissaoCalculada = CalculadoraComissao.CalculaComissao(valorDaVenda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void  TesteValorIgualA0(){
             //Arrange
            int valorDaVenda = 0;
            double comissaoEsperada = 0;
                        
            //Act
            double comissaoCalculada = CalculadoraComissao.CalculaComissao(valorDaVenda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void  TesteValorDecimal(){
             //Arrange
            double valorDaVenda = 55.59;
            double comissaoEsperada = 2.77;
                        
            //Act
            double comissaoCalculada = CalculadoraComissao.CalculaComissao(valorDaVenda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }
    }
}