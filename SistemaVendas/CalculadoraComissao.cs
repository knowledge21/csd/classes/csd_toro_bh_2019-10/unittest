using System;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraComissao
    {
        public static double CalculaComissao(double valorDaVenda){
            double percentagemComissao = (valorDaVenda <= 10000) ? 0.05: 0.06;
            double resultado = valorDaVenda * percentagemComissao;
            return Math.Floor(resultado * 100.0)/100.0;
        }
    }
}